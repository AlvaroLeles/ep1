### Informações essenciais

<p>Para o programa funcionar corretamente, as seguintes instruções devem ser respeitadas:</p>
<p>*Na pasta 'doc', no arquivo 'categorias.txt', é necessário ter pelo menos uma categoria ja cadastrada manualmente seguindo a seguinte formatação:</p>
<p>"número inteiro que representa seu ID&nbsp;&nbsp;&nbsp;nome da categoria";</p>
<p>*Na pasta 'doc', no arquivo 'clientes.txt', é necessário ter pelo menos um cliente ja cadastrado manualmente seguindo a seguinte formatação:</p>
<p>"número inteiro representa seu ID&nbsp;&nbsp;&nbsp;nome do cliente&nbsp;&nbsp;&nbsp;CPF do cliente com onze digitos sem símbolos&nbsp;&nbsp;&nbsp;telefone do cliente com oito digitos";</p>
<p>*Na pasta 'doc', no arquivo 'estoque.txt', é necessário ter pelo menos um produto ja cadastrado manualmente seguindo a seguinte formatação:</p>
<p>"número inteiro representa seu ID&nbsp;&nbsp;&nbsp;nome da categoria do produto&nbsp;&nbsp;&nbsp;nome do produto    quantidade de produtos no estoque&nbsp;&nbsp;&nbsp;preço do produto(caso seja um numero decimal, deve ser digitado com '.')";</p>
<p>**O trabalho já está sendo enviado cumprindo todas essas instruções acima, não sendo necessário modificar os arquivos manualmente ao menos que o leitor ache necessário</p>

<p>*Na tela inicial do programa é apresentado 3 opções:</p>
<p>-Modo Estoque: Mostra os produtos em estoque e permite a adição de novos produtos, alteração de quantidade ou adição de categorias.</p>
<p>-Modo Venda: Realiza venda para um cliente.</p>
<p>-Modo Recomendação: Ao inserir o nome de um cliente, permite saber a categoria favorita deste cliente e os produtos em estoque pertencentes a essa categoria.</p>

<p><strong>**Onde o programa estiver esperando palavra(s) e no item desejar-se digitar uma plavra composta, deve-se inserir as palavras com '_' no lugar de espaços, para manter a execução correta do programa, como no exemplo 'palavra_composta'**</strong></p>

<p><strong>**Não se deve digitar letra ou palavra(s) em nenhuma das partes do programa em que se espera uma entrada numérica do usuário, caso faça isso, o programa entrará em loop e sua parada deverá ser forçada por meio do "Ctrl + C" no terminal do Linux. Além disso, dependendo do ponto do programa em que esse loop acontecer serão registrados vários clientes/ou produtos/ou categorias com espaços em branco indesejados, e para resolver isso, deve-se entrar no(s) arquivo(s) .txt onde ocorreu o problema e apagar manualmente todos os elementos indesejados.**</strong></p>

<p>*No mais, o programa é bem intuitivo, e seguindo o processo do programa inserindo adequadamente as informações requisitadas, o usuário deve conseguir executar e utilizar tranquilamente o programa.</p>

### Execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

