#include "../inc/categoria.hpp"

Categoria::Categoria()
{
    nome = "";
    qtdProdutos = 0;
}

int Categoria::getId()
{
    return id;
}

void Categoria::setId(int id)
{
    if (id > 0)
    {
        this->id = id;
    }
    
}

string Categoria::getNome()
{
    return nome;
}
void Categoria::setNome(string nome)
{
    if (nome != "")
        this->nome = nome;
}
int Categoria::getQtdProdutos()
{
    return qtdProdutos;
}
void Categoria::setQtdProdutos(int qtdProdutos)
{
    if (qtdProdutos >= 0)    
        this->qtdProdutos = qtdProdutos;
}