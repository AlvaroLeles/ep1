#include "../inc/produto.hpp"

Produto::Produto()
{
    nome = "";
    Categoria categoria; 
}

Produto::~Produto()
{
    
}

int Produto::getId()
{
    return id;
}

void Produto::setId(int id)
{
    if (id >= 0)
        this ->id = id;
}

string Produto::getNome()
{
    return nome;
}

void Produto::setNome(string nome)
{
    if (nome != "")
        this->nome = nome;
}

int Produto::getQuantidade()
{
    return quantidade;
}

void Produto::setQuantidade(int quantidade)
{
    if (quantidade >= 0)
        this->quantidade = quantidade;
}

float Produto::getPreco()
{
    return preco;
}

void Produto::setPreco(float preco)
{
    if (preco > 0.0)
    {
        this->preco = preco;
    }
    
}

Categoria Produto::getCategoria()
{
    return categoria;
}

void Produto::setCategoria(Categoria categoria)
{
    this->categoria = categoria;
}