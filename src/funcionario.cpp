#include "../inc/funcionario.hpp"

Funcionario::Funcionario()
{
    funcao = "";
}

string Funcionario::getFuncao()
{
    return funcao;
}

void Funcionario::setFuncao(string funcao)
{
    if (funcao != "")
        this->funcao = funcao;
}