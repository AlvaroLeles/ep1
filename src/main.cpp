#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include "../inc/produto.hpp"
#include "../inc/cliente.hpp"
#include "../inc/categoria.hpp"
#include "../inc/funcionario.hpp"
#include "../inc/usuario.hpp"

//Mostra as categorias existentes no programa
void mostraCategorias()
{
	ifstream leitura;

	leitura.open("doc/categorias.txt");

	if (leitura.is_open())
	{
		cout << "Estes são as categorias existentes:"<< endl;
		cout << "Id        Nome" << endl;

		string linha;

		int idCategoria;
		string nomeCategoria;
		while (true)
		{
			getline(leitura, linha);
			if (linha == "")
			{
				break;
			}
			
			istringstream iss(linha);

			iss >> idCategoria >> nomeCategoria;

			cout << idCategoria << "        " << nomeCategoria << endl;
		}

		leitura.close();
	}
	cout << endl;
}

//Cadastra uma nova categoria
void cadastraCategoria()
{
	ofstream categoria;

	//Posiciona a escrita em uma nova linha branca no final do arquivo
	categoria.open("doc/categorias.txt", fstream::in | fstream::out | fstream::app);

	Categoria novaCategoria;
	string nomeNovaCategoria;
	int proxId;

	//A partir daqui, este bloco serve para pegar o ID do último produto cadastrado no arquivo e somar 1, para colocar no próximo produto
	int nroLinhas = 0;
	string linha;
	ifstream categoriaQtd("doc/categorias.txt");

	while (getline(categoriaQtd, linha))
	{
		++nroLinhas;
	}

	if (categoriaQtd.is_open())
	{
		categoriaQtd.close();
	}
	

	ifstream categoriaLeitura("doc/categorias.txt");
	for (int lineno = 0;lineno < nroLinhas; lineno++)
	{
		getline(categoriaLeitura, linha);
		if (lineno == nroLinhas-1)
		{
			//Acessa a linha do arquivo
			istringstream iss(linha);
			//Permite que eu pegue apenas o ID e descarte o resto da linha
			iss >> proxId;
			novaCategoria.setId(proxId + 1);
		}
	}
	if (categoriaLeitura.is_open())
		categoriaLeitura.close();
	
	//Acaba aqui

	cout << "Nome da categoria:\n";
	cin >> nomeNovaCategoria;
	novaCategoria.setNome(nomeNovaCategoria);

	if (categoria.is_open())
	{
		categoria << novaCategoria.getId() << "      " << novaCategoria.getNome() << endl;
		categoria.close();
	}

	cout << "Categoria cadastrada com sucesso" << endl;
}

//Carrega os dados do arquivo de estoque para a tela
void mostraEstoque()
{
	ifstream leitura;
	string texto;

	leitura.open("doc/estoque.txt");

	if (leitura.is_open())
	{
		cout << "Estes são os produtos no estoque:"<<endl;
		cout << "ID     Categoria         Nome          Quantidade      Preço(R$)" << endl;
		while (leitura >> texto)
		{
			cout << texto << "      ";
			leitura >> texto;
			cout << texto << "      ";
			leitura >> texto;
			cout << texto << "          ";
			leitura >> texto;
			cout << texto << "              ";
			leitura >> texto;
			cout << texto << endl;
		}

		leitura.close();
	}
	cout << endl;
}

//Adiciona um novo produto no arquivo
void cadastraNovoProduto()
{
	ofstream escrita;

	//Posiciona a escrita em uma nova linha branca no final do arquivo
	escrita.open("doc/estoque.txt", fstream::in | fstream::out | fstream::app);

	Produto novoProduto;
	int idCategoria;
	string nomeNovoProduto;
	int qtdNovoProduto;
	float precoNovoProduto;
	int proxId;

	//A partir daqui, este bloco serve para pegar o ID do último produto cadastrado no arquivo e somar 1, para colocar no próximo produto
	int nroLinhas = 0;
	string linha;
	ifstream estoqueQtd("doc/estoque.txt");

	while (getline(estoqueQtd, linha))
	{
		++nroLinhas;
	} 

	ifstream estoque("doc/estoque.txt");
	for (int lineno = 0;lineno < nroLinhas; lineno++)
	{
		getline(estoque, linha);
		if (lineno == nroLinhas-1)
		{
			//Acessa a linha do arquivo
			istringstream iss(linha);
			//Permite que eu pegue apenas o ID e descarte o resto da linha
			iss >> proxId;
			novoProduto.setId(proxId + 1);
		}
	}
	if (estoque.is_open())
		estoque.close();
	
	//Acaba aqui

	cout << "Nome do produto:\n";
	cin >> nomeNovoProduto;
	novoProduto.setNome(nomeNovoProduto);

	cout << "Categoria (Insira o ID correspondente):\n";
	mostraCategorias();
	cin >> idCategoria;
	string nomeCategoria;

	ifstream categorias("doc/categorias.txt");

	string linhaCategorias;
	int idCategoriaVez;
	string nomeCategoriaVez;
	
	while (true)
	{
		getline(categorias, linhaCategorias);
		if (linhaCategorias == "")
		{
			break;
		}

		istringstream iss(linhaCategorias);
		iss >> idCategoriaVez >> nomeCategoriaVez;

		if (idCategoriaVez == idCategoria)
		{
			nomeCategoria = nomeCategoriaVez;
		}
	}

	Categoria categoriaNovoProduto;
	categoriaNovoProduto.setNome(nomeCategoria);

	novoProduto.setCategoria(categoriaNovoProduto);

	cout << "Quantidade:\n";
	cin >> qtdNovoProduto;
	novoProduto.setQuantidade(qtdNovoProduto);

	cout << "Preço:\n";
	cin >> precoNovoProduto;
	novoProduto.setPreco(precoNovoProduto);

	if (escrita.is_open())
	{
		escrita << novoProduto.getId() << "      " << novoProduto.getCategoria().getNome() << "        " << novoProduto.getNome() << "       " << novoProduto.getQuantidade() << "               " << novoProduto.getPreco() << endl;
		escrita.close();
	}
}

//Altera a quantidade de um produto
void alteraQtdProduto()
{
	vector <string>linhas;

	ofstream escrita;

	int idProdutoAlterar;
	int novaQuantidade;

	cout << "Digite o ID do produto que você quer alterar a quantidade:" << endl;
	cin >> idProdutoAlterar;

	string linha = " ";
	int id;
	string nomeProduto;
	string categoriaProduto;
	string precoProduto;
	ifstream estoque("doc/estoque.txt");
	int lineno = 0;
	while (linha != "")
	{
		getline(estoque, linha);
		if (lineno == idProdutoAlterar - 1)
		{
			//Acessa a linha do arquivo
			istringstream iss(linha);

			int quantidade;

			iss >> id >> categoriaProduto >> nomeProduto >> quantidade >> precoProduto;

			if (nomeProduto == "")
			{
				cout << "ID inválido!, pressione 'ENTER' para voltar ao menu de opções";
				cin >> id;
				return;
			}
			

			cout << "Você irá alterar a quantidade de: " << nomeProduto << endl;
			cout << "Insira a nova quantidade desejada:" << endl;
			cin >> novaQuantidade;

			string linhaAlterada = to_string(id) + "        " + categoriaProduto + "		" + nomeProduto + "     " + to_string(novaQuantidade) + "              " + precoProduto;
			linhas.push_back(linhaAlterada);
		}
		else
		{
			if (linha != "")
				linhas.push_back(linha);
		}
		lineno++;
	}

	escrita.open("doc/estoque.txt");
	if (escrita.is_open())
	{
		for (int i = 0; i < linhas.size(); i++)
		{
			escrita << linhas[i] << endl;
		}
		escrita.close();
	}
}

//Altera a quantidade dos produtos em estoque com base no que foi comprado
void alteraQtdProduto(vector<int> idsProdutosAlterar, vector<int> qtdsSubtrair)
{
	ifstream estoqueConsulta("doc/estoque.txt");

	string linhaConsulta;
	int idProduto;
	string categoriaProdutoConsulta;
	string nomeProdutoConsulta;
	int qtdProduto;

	vector<int> novasQuantidades(9999999, 0);
	for(int i = 0; ; i++)
	{
		getline(estoqueConsulta, linhaConsulta);

		if(linhaConsulta == "")
		{
			break;
		}

		istringstream iss(linhaConsulta);

		iss >> idProduto >> categoriaProdutoConsulta >> nomeProdutoConsulta >> qtdProduto;

		for(int j = 0; j <= idsProdutosAlterar.size(); j++)
		{
			if (idProduto == idsProdutosAlterar[j])
			{
				novasQuantidades[j] = qtdProduto - qtdsSubtrair[j];
				break;
				if (novasQuantidades[j] < 0)
				{
					throw(-5);
				}            
			}
		}		
	}

	if (estoqueConsulta.is_open())
	{
		estoqueConsulta.close();
	}

	vector <string>linhas;

	ofstream escrita;

	string linha;
	int id;
	string categoriaProduto;
	string nomeProduto;
	string precoProduto;
	ifstream estoque("doc/estoque.txt");

	for(int lineno = 0; ;lineno++)
	{
		getline(estoque, linha);

		if (linha == "")
		{
			break;
		}

		istringstream iss(linha);
		iss >> id >> categoriaProduto >> nomeProduto >> qtdProduto >> precoProduto;

		for (int i = 0; i < idsProdutosAlterar.size(); i++)
		{
			if (id == idsProdutosAlterar[i])
			{
				string linhaAlterada = to_string(id) + "        " + categoriaProduto + "		" + nomeProduto + "     " + to_string(novasQuantidades[i]) + "              " + precoProduto;
				linhas.push_back(linhaAlterada);
				break;
			}
			
			else if (i == idsProdutosAlterar.size() - 1 && id != idsProdutosAlterar[i])
			{
				linhas.push_back(linha);
			}
		}
	}

	if (estoque.is_open())
	{
		estoque.close();
	}

	escrita.open("doc/estoque.txt");
	if (escrita.is_open())
	{
		for (int i = 0; i < linhas.size(); i++)
		{
			escrita << linhas[i] << endl;
		}
		escrita.close();
	}
}

int clienteCadastrado = 0; //Variavél global para informar se o cliente que está sendo atendido é cadastrado ou não
string nomeClienteAtendido; //Variável global que contém o nome do cliente que está sendo atendido

//Verifica se o CPF informado existe no arquivo de clientes
int verificaCadastro()
{
	long int CPFinformado;
	cout << "Informe o CPF para que possamos confirmar o cadastro:" << endl;
	cin >> CPFinformado;

	ifstream leitura;
	string linha = " ";
	leitura.open("doc/clientes.txt");

	int id;
	string nome;
	long int CPF;
	short clienteEncontrado = 0;
	while (linha != "")
	{
		getline(leitura, linha);
		istringstream iss(linha);

		iss >> id >> nome >> CPF;
		if (CPF == CPFinformado)
		{
			cout << "Nome do cliente: " << nome << endl;
			clienteEncontrado = 1;
			clienteCadastrado = 1;
			nomeClienteAtendido = nome;
			break;
		}
	}
	if (clienteEncontrado == 0)
		cout << "Este CPF não se encontra em nossa base de dados, o programa se encerrará!" << endl;

	return clienteEncontrado;
}

//Cadastra um novo cliente
void cadastraCliente()
{
	ofstream escrita;

	escrita.open("doc/clientes.txt", fstream::in | fstream::out | fstream::app);

	Cliente novoCliente;
	string nomeNovoCliente;
	long int CPFNovoCliente;
	long int telefoneNovoCliente;
	int proxId;

	int nroLinhas = 0;
	string linha;
	ifstream qtdClientes("doc/clientes.txt");

	while (getline(qtdClientes, linha))
	{
		++nroLinhas;
	}

	if (qtdClientes.is_open())
	{
		qtdClientes.close();
	}
	

	ifstream clientes("doc/clientes.txt");
	for (int lineno = 0;lineno < nroLinhas; lineno++)
	{
		getline(clientes, linha);
		if (lineno == nroLinhas-1)
		{
			//Acessa a linha do arquivo
			istringstream iss(linha);
			//Permite que eu pegue apenas o ID e descarte o resto da linha
			iss >> proxId;
			proxId++;
		}
	}
	if (clientes.is_open())
		clientes.close();
	
	cout << "Insira o nome do cliente:\n";
	cin >> nomeNovoCliente;
	novoCliente.setNome(nomeNovoCliente);

	cout << "Insira o CPF do cliente:\n";
	cin >> CPFNovoCliente;
	novoCliente.setCPF(CPFNovoCliente);

	cout << "Insira o telefone do cliente:\n";
	cin >> telefoneNovoCliente;
	novoCliente.setTelefone(telefoneNovoCliente);

	if (escrita.is_open())
	{
		escrita << proxId << "      " << novoCliente.getNome() << "    " << novoCliente.getCPF() << "      " << novoCliente.getTelefone() << endl;
		escrita.close();
	}

	clienteCadastrado = 1;
	nomeClienteAtendido = novoCliente.getNome();

	string caminhoArquivoCategoriaCliente = "doc/" + novoCliente.getNome() + ".txt";
	ofstream arquivoNovoCliente(caminhoArquivoCategoriaCliente);

	ifstream categorias("doc/categorias.txt");
	string linhaCategorias;
	int qtdCategorias = 0;

	while (true)
	{
		getline(categorias, linhaCategorias);

		if (linhaCategorias == "")
		{
			break;
		}
		qtdCategorias++;
	}

	if (categorias.is_open())
	{
		categorias.close();
	}

	for (int i = 1; i <= qtdCategorias; i++)
	{
		arquivoNovoCliente << to_string(i) + "    " + to_string(0) << endl;
	}
}

void mostraClientes()
{
	ifstream leitura;

	leitura.open("doc/clientes.txt");

	if (leitura.is_open())
	{
		cout << "Estes são os clientes registrados no sistema:"<< endl;
		cout << "ID    Nome        CPF" << endl;

		string linha;

		int idCliente;
		string nomeCliente;
		long int CPFCliente;
		long int telefone;
		while (true)
		{
			getline(leitura, linha);
			if (linha == "")
			{
				break;
			}
			
			istringstream iss(linha);

			iss >> idCliente >> nomeCliente >> CPFCliente;

			cout << idCliente << "     " << nomeCliente << "      " << CPFCliente << endl;
		}

		leitura.close();
	}
	cout << endl;
}

//Atualiza o arquivo de preferencias do cliente
void atualizaArquivoCategoriaCliente()
{
	ifstream carrinho("doc/carrinho.txt");
	ifstream categorias("doc/categorias.txt");

	string linhaCarrinho;
	int idProduto;
	string nomeCategoriaCarrinho;
	while (true)
	{
		linhaCarrinho = "";
		idProduto = -1;
		nomeCategoriaCarrinho = "";
		getline(carrinho, linhaCarrinho);

		if (linhaCarrinho == "")
		{
			break;
		}
		
		istringstream issCarrinho(linhaCarrinho);
		issCarrinho >> idProduto >> nomeCategoriaCarrinho;

		int idCategoriaDesejado = 0;

		string linhaCategorias;
		int idCategoria;
		string nomeCategoriaCategorias;
		while (true)
		{
			getline(categorias, linhaCategorias);

			if (linhaCategorias == "")
			{
				break;
			}
			
			istringstream issCategorias(linhaCategorias);
			issCategorias >> idCategoria >> nomeCategoriaCategorias;

			if (nomeCategoriaCategorias == nomeCategoriaCarrinho)
			{
				idCategoriaDesejado = idCategoria;
				break;
			}
		}

		if (categorias.is_open())
		{
			categorias.close();
		}
		

		string nomeArquivoCliente = "doc/" + nomeClienteAtendido + ".txt";
		ifstream arquivoCategoriasCliente(nomeArquivoCliente);

		string linhaCategoriasCliente;
		int idCategoriaArquivoCliente;
		int qtdCategoria;

		vector<string> linhasCategoriasCliente;
		while (true)
		{
			getline(arquivoCategoriasCliente, linhaCategoriasCliente);

			if (linhaCategoriasCliente == "")
			{
				break;
			}
			
			istringstream issCategoriasCliente(linhaCategoriasCliente);
			issCategoriasCliente >> idCategoriaArquivoCliente >> qtdCategoria;

			if (idCategoriaArquivoCliente == idCategoriaDesejado)
			{
				qtdCategoria++;
				string novaLinha = to_string(idCategoriaArquivoCliente) + "    " + to_string(qtdCategoria);
				linhasCategoriasCliente.push_back(novaLinha);
			}
			else
			{
				linhasCategoriasCliente.push_back(linhaCategoriasCliente);
			}
		}

		if (arquivoCategoriasCliente.is_open())
		{
			arquivoCategoriasCliente.close();
		}
		
		ofstream arquivoCategoriasClienteEscrita(nomeArquivoCliente);

		if(arquivoCategoriasClienteEscrita.is_open())
		{
			for (int i = 0; i < linhasCategoriasCliente.size(); i++)
			{
				arquivoCategoriasClienteEscrita << linhasCategoriasCliente[i] << endl;
			}

			arquivoCategoriasClienteEscrita.close();
		}
	}

	if (carrinho.is_open())
	{
		carrinho.close();
	}
}

//Limpa o arquivo 'carrinho.txt'
void limpaArquivoCarrinho()
{
	ofstream carrinho;
	carrinho.open("doc/carrinho.txt", ofstream::out | ofstream::trunc);
	carrinho.close();
}

//Adiciona um produto ao arquivo do carrinho
void adicionaProdutoCarrinho()
{
	cout << "Selecione, informando o ID, qual produto você deseja adicionar ao carrinho, e, em seguida, informe a quantidade" << endl;

	int idProduto;
	int quantidadeDesejada;

	cout << "ID do produto:" << endl;
	cin >> idProduto;
	cout << "Quantidade desejada:" << endl;
	cin >> quantidadeDesejada;

	ifstream estoque;
	estoque.open("doc/estoque.txt");
	string linha;
	int idProdutoEscolhido = -1;
	string nomeCategoriaProdutoEscolhido;
	string nomeProdutoEscolhido;
	float precoProdutoEscolhido;
	while (idProdutoEscolhido != idProduto)
	{
		getline(estoque, linha);
		istringstream iss(linha);

		int quantidadeProdutoEscolhido; //Esta variavel está sendo criada apenas para se poder pegar o preço do produto e será descartada

		iss >> idProdutoEscolhido >> nomeCategoriaProdutoEscolhido >> nomeProdutoEscolhido >> quantidadeProdutoEscolhido >> precoProdutoEscolhido;
	}

	if (estoque.is_open())
	{
		estoque.close();
	}
	
			
	ofstream escritaCarrinho;

	//Posiciona a escrita em uma nova linha branca no final do arquivo
	escritaCarrinho.open("doc/carrinho.txt", fstream::in | fstream::out | fstream::app);

	Produto produtoEscolhido;
	Categoria categoriaProdutoEscolhido;
	categoriaProdutoEscolhido.setNome(nomeCategoriaProdutoEscolhido);

	produtoEscolhido.setId(idProdutoEscolhido);
	produtoEscolhido.setNome(nomeProdutoEscolhido);
	produtoEscolhido.setQuantidade(quantidadeDesejada);
	produtoEscolhido.setPreco(precoProdutoEscolhido);
	produtoEscolhido.setCategoria(categoriaProdutoEscolhido);

	if (escritaCarrinho.is_open())
	{
		escritaCarrinho << produtoEscolhido.getId() << "        " << produtoEscolhido.getCategoria().getNome() << "        " << produtoEscolhido.getNome() << "       " << produtoEscolhido.getQuantidade() << "        " << produtoEscolhido.getPreco() << endl;
		escritaCarrinho.close();
	}
}

//Mostra os produto atuais do carrinho
void mostraCarrinho()
{
	ifstream leitura;

	leitura.open("doc/carrinho.txt");

	if (leitura.is_open())
	{
		cout << "Estes são os produtos no carrinho:"<< endl;
		cout << "Nome        Quantidade      Preço Unid.(R$)" << endl;

		string linha;

		int idProduto;
		string categoriaProduto;
		string nomeProduto;
		int qtdProduto;
		float precoProduto;
		while (true)
		{
			getline(leitura, linha);
			if (linha == "")
			{
				break;
			}
			
			istringstream iss(linha);

			iss >> idProduto >> categoriaProduto >> nomeProduto >> qtdProduto >> precoProduto;

			cout << nomeProduto << "        " << qtdProduto << "            " << precoProduto << endl;
		}

		leitura.close();
	}
	cout << endl;
}

//Identifica quais e quantos produtos estão no carrinho para removê-los do estoque
void abateProdutosEstoque()
{
	vector<int> idsCarrinho;
	vector<int> qtdsProdutoCarrinho;
	ifstream carrinho("doc/carrinho.txt");

	string linha;

	int idProduto;
	string categoriaProduto;
	string nomeProduto;
	int qtdProduto;

	while (true)
	{
		getline(carrinho, linha);
		if (linha == "")
		{
			break;
		}
		
		istringstream iss(linha);

		iss >> idProduto >> categoriaProduto >> nomeProduto >> qtdProduto;
		idsCarrinho.push_back(idProduto);
		qtdsProdutoCarrinho.push_back(qtdProduto);
	}
		alteraQtdProduto(idsCarrinho, qtdsProdutoCarrinho);

		if (clienteCadastrado == 1)
		{
			atualizaArquivoCategoriaCliente();
		}
		
}

//Calcula o valor final da compra
void calculaValorFinalCompra()
{
	abateProdutosEstoque();

	ifstream carrinho("doc/carrinho.txt");

	float valorFinal = 0.0;

	string linha;
	while (true)
	{
		getline(carrinho, linha);

		if (linha == "")
		{
			break;
		}
		
		istringstream iss(linha);

		int idProduto;
		string categoriaProduto;
		string nomeProduto; //Esta variavel está sendo criada apenas para se poder pegar a quantidade e o preço do produto e será descartada
		int qtdProduto; 
		float precoProduto;

		iss >> idProduto >> categoriaProduto >> nomeProduto >> qtdProduto >> precoProduto;
		valorFinal += qtdProduto * precoProduto;
	}

	if (carrinho.is_open())
	{
		carrinho.close();
	}

	cout << "Valor final da compra: R$" << valorFinal << endl;
	if (clienteCadastrado == 1)
	{
		float desconto = valorFinal * 15 / 100;
		float valorFinalDesconto = valorFinal - desconto;

		cout << "Por ser um cliente cadastrado, há um desconto de 15% aplicado ao valor final da compra. (R$"<< desconto << ")" << endl << "O novo valor valor da compra é: R$" << valorFinalDesconto << endl;
	}
}

//Parte do programa que rodará caso o usuário seja um vendedor
void modoEstoque()
{
	system("clear");
	ofstream escrita;
	ifstream leitura;

	mostraEstoque();

	int escolhaAlteraEstoque = 99;

	while (escolhaAlteraEstoque != 0)
	{
		cout << "Deseja cadastrar um novo produto ou alterar sua quantidade, ou cadastrar uma nova categoria? (1-Novo produto/2-Alterar produto existente/3-Nova categoria/0-Não fazer modificações)" << endl;
		cin >> escolhaAlteraEstoque;

		if (escolhaAlteraEstoque == 1)
		{
			system("clear");
			cadastraNovoProduto();
			system("clear");
			mostraEstoque();
		}
		else if (escolhaAlteraEstoque == 2)
		{
			cout << endl;
			alteraQtdProduto();
			system("clear");
			mostraEstoque();
		}
		else if (escolhaAlteraEstoque == 3)
		{
			system("clear");
			mostraCategorias();
			cout << endl;
			cadastraCategoria();
		}
		
		
	}
}

//Parte do programa que rodará caso o usuário seja um cliente
void modoVenda()
{
	system("clear");
	int possuiCadastro;
	cout << "O cliente já possui cadastro na loja? (1- Sim / 2- Não)" << endl;
	cin >> possuiCadastro;

	if (possuiCadastro == 1)
	{
		if(verificaCadastro() != 1)
		{
		   return;
		}
	}
	else if (possuiCadastro == 2)
	{
		cout << "O cliente deseja fazer cadastro? (Informar sobre os 15% de desconto para clientes cadastrados) (1- Sim / 2-Não)" << endl;
		int desejaCadastro;
		cin >> desejaCadastro;
		if (desejaCadastro == 1)
		{
			cadastraCliente();
			system("clear");
		}        
	}
	else
	{
		cout << "Uma opção inválida foi escolhida, o programa se encerrará!" << endl;
		return;
	}

	mostraEstoque();
	adicionaProdutoCarrinho();

	int encerrarCompra = 1;
	while (true)
	{
		cout << "Deseja adicionar mais algum produto ao carrinho ou podemos encerrar a compra? (1- Adicionar novo produto ao carrinho / 2- Encerrar a compra)" << endl;
		cin >> encerrarCompra;

		if (encerrarCompra == 2)
		{
			system("clear");
			break;
		}
		
		if (encerrarCompra != 1 && encerrarCompra != 2)
		{
			cout << "Uma opção inválida foi escolhida, vamos encerrar a compra!" << endl;
			break;
		}

		mostraEstoque();
		adicionaProdutoCarrinho();
		
	}

	try
	{
		mostraCarrinho();
		calculaValorFinalCompra();
	}
	catch(int e)
	{
		cout << "Um ou mais produtos no carrinho possuem uma quantidade maior do que tem no estoque, o programa será encerrado e a venda não será computada!" << endl;
		return;
	}
}

//Parte do programa que rodará para ver os produtos recomendados de um certo cliente
void modoRecomendacao()
{
	system("clear");
	mostraClientes();
	string nomeCliente;
	cout << "Insira o nome do cliente (respeitando a maneira como está salvo no sistema) para ver os produtos recomendados para ele:" << endl;
	cin >> nomeCliente;

	string arquivoCliente = "doc/" + nomeCliente + ".txt";
	ifstream cliente(arquivoCliente);
	if (cliente.fail())
	{
		cout << "Este cliente não está registrado na base de dados, o programa se encerrará!" << endl;
		return;
	}
	
	ifstream categorias("doc/categorias.txt");

	string linhaCliente;
	int idCategoria;
	int pesoCategoria;
	int maiorPeso = -1;
	int idMaiorPeso;
	while (true)
	{
		getline(cliente, linhaCliente);
		if (linhaCliente == "")
		{
			break;
		}

		istringstream issCliente(linhaCliente);

		issCliente >> idCategoria >> pesoCategoria;

		if (pesoCategoria > maiorPeso)
		{
			idMaiorPeso = idCategoria;
			maiorPeso = pesoCategoria;
		}
	}

	if (cliente.is_open())
	{
		cliente.close();
	}
	
	string linhaCategorias;
	int idCategoriaVez;
	string nomeCategoria;
	string categoriaRecomendada;
	while (true)
	{
		getline(categorias, linhaCategorias);

		if (linhaCategorias == "")
		{
			break;
		}

		istringstream issCategorias(linhaCategorias);

		issCategorias >> idCategoriaVez >> nomeCategoria;

		if (idCategoriaVez == idMaiorPeso)
		{
			categoriaRecomendada = nomeCategoria;
		}
		
	}

	if (categorias.is_open())
	{
		categorias.close();
	}

	cout << "O cliente " << nomeCliente << " tem preferência por produtos da categoria " << categoriaRecomendada << "." << endl;

	cout << "O cliente pode se interessar pelos seguintes produtos:" << endl;
	ifstream estoque("doc/estoque.txt");

	string linhaEstoque;
	int idProduto;
	string nomeCategoriaEstoque;
	string nomeProduto;
	while (true)
	{
		getline(estoque, linhaEstoque);

		if(linhaEstoque == "")
		{
			break;
		}

		istringstream issEstoque(linhaEstoque);

		issEstoque >> idProduto >> nomeCategoriaEstoque >> nomeProduto;
		if (nomeCategoriaEstoque == categoriaRecomendada)
		{
			cout << nomeProduto << endl;
		}
	}

	if (estoque.is_open())
	{
		estoque.close();
	}
}

int main()
{
	system("clear");

	int modoPrograma = -1;
	cout << "Victoria's" << endl;
	cout << "0- Modo Estoque" << endl;
	cout << "1- Modo Venda" << endl;
	cout << "2- Modo Recomendação" << endl;
	cout << "Insira o modo em que deseja operar: " << endl;
	cin >> modoPrograma;

	while (modoPrograma != 0 && modoPrograma != 1 && modoPrograma != 2)
	{
		cout << "Escolha uma opção válida\n";
		cin >> modoPrograma;
	}

	if (modoPrograma == 0)
	{
		modoEstoque();        
	}
	else if (modoPrograma == 1)
	{
		modoVenda();
	}
	else if (modoPrograma == 2)
	{
		modoRecomendacao();
	}
	

	limpaArquivoCarrinho();

	return 0;
}