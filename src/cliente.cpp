#include "../inc/cliente.hpp"

Cliente::Cliente()
{
    CPF = 0;
    telefone = 0;
}

long int Cliente::getCPF()
{
    return CPF;
}

void Cliente::setCPF(long int CPF)
{
    if (CPF >= 11111111111 || CPF <= 99999999999)
        this->CPF = CPF;
}

long int Cliente::getTelefone()
{
    return telefone;
}

void Cliente::setTelefone(long int telefone)
{
    if (telefone >= 11111111 || telefone <= 99999999)
        this->telefone = telefone;
}