#ifndef CATEGORIA_HPP
#define CATEGORIA_HPP

#include <string>

using namespace std;

class Categoria
{
    private:
        int id;
        string nome;
        int qtdProdutos;
    public:
        Categoria();
        int getId();
        void setId(int id);
        string getNome();
        void setNome(string nome);
        int getQtdProdutos();
        void setQtdProdutos(int qtdProdutos);
};

#endif