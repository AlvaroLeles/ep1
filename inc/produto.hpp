#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include "categoria.hpp"

class Produto
{
    private:
        int id;
        string nome;
        int quantidade;
        float preco;
        Categoria categoria;
    public:
        Produto();
        ~Produto();
        int getId();
        void setId(int id);
        string getNome();
        void setNome(string nome);
        int getQuantidade();
        void setQuantidade(int quantidade);
        float getPreco();
        void setPreco(float preco);
        Categoria getCategoria();
        void setCategoria(Categoria categoria);
};

#endif