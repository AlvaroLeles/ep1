#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "usuario.hpp"

using namespace std;

class Cliente: public Usuario
{
private:
    long int CPF;
    long int telefone;
public:
    Cliente();
    long int getCPF();
    void setCPF(long int CPF);
    long int getTelefone();
    void setTelefone(long int Telefone);
};

#endif