#ifndef USUARIO_HPP
#define USUARIO_HPP

#include<string>

using namespace std;

class Usuario
{
    protected:
        string nome;
    public:
        Usuario();
        string getNome();
        void setNome(string nome);
};

#endif