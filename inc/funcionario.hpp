#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include "usuario.hpp"

using namespace std;

class Funcionario: public Usuario
{
private:
    string funcao;
public:
    Funcionario();
    string getFuncao();
    void setFuncao(string Funcao);
};

#endif